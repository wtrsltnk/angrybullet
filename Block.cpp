/* 
 * File:   Block.cpp
 * Author: wouter
 * 
 * Created on March 17, 2012, 1:30 PM
 */

#include "Block.h"
#include "Tokenizer.h"
#include "TextureLoader.h"
#include "Physics.h"
#include <string>
#include <iostream>

#define PI 3.14159265358979323846
#define DEG2RAD(DEG) ((DEG)*((PI)/(180.0)))

Block::Block()
	: mBlockMaterial(BlockMaterial::Wood)
{ }

Block::~Block()
{ }

BlockMaterial::Type Block::getBlockMaterial()
{
	return this->mBlockMaterial;
}

void Block::setBlockMaterial(BlockMaterial::Type material)
{
	this->mBlockMaterial = material;
}

void Block::saveToStream(std::ofstream& stream)
{
    stream << "{" << std::endl;
    stream << "\"class\" \"Block\"" << std::endl;
    stream << "\"mass\" \"" << this->mMass << "\"" << std::endl;
    stream << "\"position\" \""
           << this->getPosition().x() << " "
           << this->getPosition().y() << " "
           << this->getPosition().z() << "\""
           << std::endl;
    stream << "\"orientation\" \""
           << this->getOrientation().x() << " "
           << this->getOrientation().y() << " "
           << this->getOrientation().z() << " "
           << this->getOrientation().w() << "\""
           << std::endl;
	if (this->getTexture() != 0)
        stream << "\"texture\" \"" << this->getTexture()->mName << "\"" << std::endl;
    stream << "\"material\" \"" << (this->mBlockMaterial == BlockMaterial::Stone ? "stone" : "wood") << "\"" << std::endl;
    stream << "}" << std::endl;
}

void Block::loadFromTokenizer(Tokenizer& tok)
{
    while (tok.nextToken() && tok.getToken() != std::string("}"))
	{
        if (tok.getToken() == std::string("mass"))
		{
			this->mMass = atof(tok.getNextToken());
		}
        else if (tok.getToken() == std::string("position"))
		{
			tok.nextToken();
			float x = 0.0f, y = 0.0f, z = 0.0f;
			sscanf(tok.getToken(), "%f %f %f", &x, &y, &z);
			this->mPosition = btVector3(x, y, z);
		}
        else if (tok.getToken() == std::string("orientation"))
		{
			tok.nextToken();
			float x = 0.0f, y = 0.0f, z = 0.0f, w = 0.0f;
			sscanf(tok.getToken(), "%f %f %f %f", &x, &y, &z, &w);
			this->mOrientation = btQuaternion(x, y, z, w);
		}
        else if (tok.getToken() == std::string("texture"))
		{
			TextureLoader tl;
			this->setTexture(tl.loadFromTga(tok.getNextToken()));
			if (this->getTexture() != 0)
            {
                float parameters[6] = { static_cast<float>(this->getTexture()->mWidth), 50, static_cast<float>(this->getTexture()->mHeight), 0, 0, 0 };
				this->setBoundingVolume(GameObject::Box, parameters);
			}
		}
        else if (tok.getToken() == std::string("material"))
		{
			tok.nextToken();
            if (tok.getToken() == std::string("stone"))
				this->mBlockMaterial = BlockMaterial::Stone;
            if (tok.getToken() == std::string("wood"))
				this->mBlockMaterial = BlockMaterial::Wood;
		}
	}
}

GameObject* Block::clone()
{
	return new Block(*this);
}

Block* Block::fromTemplate(Tokenizer& tok)
{
	Block* block = new Block();

    while (tok.nextToken() && tok.getToken() != std::string("}"))
	{
        std::string key(tok.getToken());
        std::string value(tok.getNextToken());

		if (key == "texture")
		{
			TextureLoader tl;
            Texture* texture = tl.loadFromTga(value);
			block->setTexture(texture);
            float parameters[6] = { static_cast<float>(texture->mWidth), 50, static_cast<float>(texture->mHeight), 0, 0, 0 };
			block->setBoundingVolume(GameObject::Box, parameters);
			block->getOrientation().setRotation(btVector3(0, 1, 0), DEG2RAD(90));
		}
		else if (key == "mass")
		{
			block->setMass(atof(value.c_str()));
		}
		else if (key == "material")
		{
			if (value == "stone")
				block->setBlockMaterial(BlockMaterial::Stone);
			else if (value == "wood")
				block->setBlockMaterial(BlockMaterial::Wood);
		}
	}
	return block;
}
