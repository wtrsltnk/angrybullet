/* 
 * File:   Block.h
 * Author: wouter
 *
 * Created on March 17, 2012, 1:30 PM
 */

#ifndef BLOCK_H
#define	BLOCK_H

#include "GameObject.h"

namespace BlockMaterial
{
	enum Type
	{
		Stone,
		Wood
	};
}

class Block : public GameObject
{
public:
	Block();
	virtual ~Block();
	
	BlockMaterial::Type getBlockMaterial();
	void setBlockMaterial(BlockMaterial::Type material);
	
    virtual void saveToStream(std::ofstream& stream);
	virtual void loadFromTokenizer(Tokenizer& tok);
	virtual GameObject* clone();
	
private:
	BlockMaterial::Type mBlockMaterial;

public:
	static Block* fromTemplate(Tokenizer& tok);
};

#endif	/* BLOCK_H */

