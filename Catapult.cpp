/* 
 * File:   Catapult.cpp
 * Author: wouter
 * 
 * Created on March 10, 2012, 12:34 PM
 */

#include "Catapult.h"
#include "Projectile.h"
#include "TextureLoader.h"
#include <GL/gl.h>
#include <iostream>

Catapult::Catapult()
	: mIsCaptured(false)
{
	float bb[] = { 10,10, 10 };
	this->setBoundingVolume(GameObject::Box, bb);
}

Catapult::~Catapult()
{ }

void Catapult::capture(int x, int y)
{
	this->mIsCaptured = true;
	this->mVectorX = x;
	this->mVectorY = y;
}

GameObject* Catapult::uncapture(btVector3& linvel)
{
	Projectile* obj = new Projectile();
	linvel = btVector3(this->mVectorX - this->mPosition[0], 0, this->mVectorY - this->mPosition[2]);
	obj->setPosition(btVector3(this->mVectorX, 0, this->mVectorY));
	float b2[] = { 10, 10, 10 };
	obj->setBoundingVolume(GameObject::Box, b2);
	
	this->mIsCaptured = false;
	
	return obj;
}

void Catapult::move(int x, int y)
{
	this->mVectorX = x;
	this->mVectorY = y;
}

void Catapult::render()
{
	if (this->mIsCaptured)
	{
		glPushMatrix();
//		btTransform t = this->getTransform();
//		float m[16] = { 0 };
//		t.getOpenGLMatrix(m);
//		glMultMatrixf(m);
	
		glBegin(GL_LINES);
		glVertex3f(this->mPosition[0], this->mPosition[1], this->mPosition[2]);
		glVertex3f(this->mVectorX, 0, this->mVectorY);
		glEnd();
		
		glTranslatef(this->mVectorX, 0, this->mVectorY);
		if (this->mBoundingType == GameObject::Box)
		{
			// Draw a cube
			glBegin(GL_QUADS);

				glVertex3f(-this->mBoundingParameters[0],  this->mBoundingParameters[1],  this->mBoundingParameters[2]);
				glVertex3f(-this->mBoundingParameters[0],  this->mBoundingParameters[1], -this->mBoundingParameters[2]);
				glVertex3f( this->mBoundingParameters[0],  this->mBoundingParameters[1], -this->mBoundingParameters[2]);
				glVertex3f( this->mBoundingParameters[0],  this->mBoundingParameters[1],  this->mBoundingParameters[2]);

			glEnd();
		}


		glPopMatrix();
	}
	else
		GameObject::render();
}

void Catapult::saveToStream(std::ofstream& stream)
{
    stream << "{" << std::endl;
    stream << "\"class\" \"Catapult\"" << std::endl;
    stream << "\"mass\" \"" << this->mMass << "\"" << std::endl;
    stream << "\"position\" \""
           << this->getPosition().x() << " "
           << this->getPosition().y() << " "
           << this->getPosition().z() << "\""
           << std::endl;
    stream << "\"orientation\" \""
           << this->getOrientation().x() << " "
           << this->getOrientation().y() << " "
           << this->getOrientation().z() << " "
           << this->getOrientation().w() << "\""
           << std::endl;
    if (this->getTexture() != 0)
        stream << "\"texture\" \"" << this->getTexture()->mName << "\"" << std::endl;
    stream << "}" << std::endl;
}

void Catapult::loadFromTokenizer(Tokenizer& tok)
{
    while (tok.nextToken() && tok.getToken() != std::string("}"))
	{
        if (tok.getToken() == std::string("mass"))
		{
			this->mMass = atof(tok.getNextToken());
		}
        else if (tok.getToken() == std::string("position"))
		{
			float x = atof(tok.getNextToken());
			float y = atof(tok.getNextToken());
			float z = atof(tok.getNextToken());
			this->mPosition = btVector3(x, y, z);
		}
        else if (tok.getToken() == std::string("orientation"))
		{
			float x = atof(tok.getNextToken());
			float y = atof(tok.getNextToken());
			float z = atof(tok.getNextToken());
			float w = atof(tok.getNextToken());
			this->mOrientation = btQuaternion(x, y, z, w);
		}
        else if (tok.getToken() == std::string("texture"))
		{
			TextureLoader tl;
			this->setTexture(tl.loadFromTga(tok.getNextToken()));
			if (this->getTexture() != 0)
			{
                float parameters[6] = { static_cast<float>(this->getTexture()->mWidth), 50, static_cast<float>(this->getTexture()->mHeight), 0, 0, 0 };
				this->setBoundingVolume(GameObject::Box, parameters);
			}
		}
	}
}

GameObject* Catapult::clone()
{
	return new Catapult(*this);
}
