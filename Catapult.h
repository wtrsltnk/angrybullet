/* 
 * File:   Catapult.h
 * Author: wouter
 *
 * Created on March 10, 2012, 12:34 PM
 */

#ifndef CATAPULT_H
#define	CATAPULT_H

#include "GameObject.h"

class Catapult : public GameObject
{
public:
	Catapult();
	virtual ~Catapult();
	
	void capture(int x, int y);
	GameObject* uncapture(btVector3& linvel);
	void move(int x, int y);
	
	bool isCaptured() { return this->mIsCaptured; }
	
	virtual void render();
	
    virtual void saveToStream(std::ofstream& stream);
	virtual void loadFromTokenizer(Tokenizer& tok);
	virtual GameObject* clone();
	
private:
	bool mIsCaptured;
	int mVectorX, mVectorY;

};

#endif	/* CATAPULT_H */

