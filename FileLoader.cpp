/* 
 * File:   FileLoader.cpp
 * Author: wouter
 * 
 * Created on November 28, 2010, 10:34 AM
 */

#include "FileLoader.h"
#include <stdio.h>
#include <string.h>

FileLoader::FileLoader()
{ }

FileLoader::~FileLoader()
{ }

bool FileLoader::loadBinaryFile(const std::string& filename, byte*& out, long& len)
{
    FILE* file = fopen(filename.c_str(), "r");
    if (file != nullptr)
    {
        fseek(file, 0, SEEK_END);
        len = ftell(file);
        fseek(file, 0, SEEK_SET);
        out = new byte[len];
        fread(out, 1, static_cast<size_t>(len), file);
        fclose(file);
        return true;
    }
    return false;
}

const std::string FileLoader::getExtension(const std::string& filename)
{
    auto found = filename.find_last_of('.');

    if (found == std::string::npos) return "";

    return filename.substr(found);
}
