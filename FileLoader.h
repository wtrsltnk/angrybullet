/* 
 * File:   FileLoader.h
 * Author: wouter
 *
 * Created on November 28, 2010, 10:34 AM
 */

#ifndef FILELOADER_H
#define	FILELOADER_H

#include <string>

typedef unsigned char byte;

class FileLoader
{
public:
    FileLoader();
    virtual ~FileLoader();

    bool loadBinaryFile(const std::string& filename, byte*& out, long& len);

    static const std::string getExtension(const std::string& filename);
};

#endif	/* FILELOADER_H */

