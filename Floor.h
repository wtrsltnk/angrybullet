/* 
 * File:   Floor.h
 * Author: wouter
 *
 * Created on March 10, 2012, 11:45 AM
 */

#ifndef FLOOR_H
#define	FLOOR_H

#include "GameObject.h"

class Floor : public GameObject
{
public:
	Floor();
	virtual ~Floor();
	
    virtual void saveToStream(std::ofstream& stream);
	virtual void loadFromTokenizer(Tokenizer& tok);
	virtual GameObject* clone();
	
private:

};

#endif	/* FLOOR_H */

