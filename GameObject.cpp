/* 
 * File:   GameObject.cpp
 * Author: wouter
 * 
 * Created on December 14, 2011, 6:10 PM
 */

#include "GameObject.h"
#include "Physics.h"
#include <GL/gl.h>

GameObject::GameObject()
    : mPosition(btVector3(0, 0, 0)), mOrientation(0, 0, 0, 1), mBoundingType(GameObject::Sphere), mMass(1.0f), mTexture(0)
{
	this->mBoundingParameters[0] = 5;
	this->saveState();
}

GameObject::GameObject(Texture* texture)
    : mPosition(btVector3(0, 0, 0)), mOrientation(0, 0, 0, 1), mBoundingType(GameObject::Sphere), mTexture(texture)
{
	this->mBoundingParameters[0] = 5;
	this->saveState();
}

GameObject::GameObject(const GameObject& obj)
    : mPosition(obj.mPosition), mOrientation(obj.getOrientation()), mBoundingType(obj.getBoundingType()), mMass(obj.mMass), mTexture(obj.mTexture)
{
	for (int i = 0; i < 6; i++)
		this->mBoundingParameters[i] = obj.mBoundingParameters[i];
	this->saveState();
}

GameObject::~GameObject()
{ }

void GameObject::render()
{
	glPushMatrix();
	btTransform t = this->getTransform();
	float m[16] = { 0 };
	t.getOpenGLMatrix(m);
	glMultMatrixf(m);
	
	if (this->mTexture != 0)
	{
		glEnable(GL_TEXTURE_2D);
		this->mTexture->use();
	}
	else
		glDisable(GL_TEXTURE_2D);
	
	if (this->mBoundingType == GameObject::Box)
	{
		// Draw a cube
		glBegin(GL_QUADS);

			glTexCoord2f(0, 1); glVertex3f(-this->mBoundingParameters[0],  this->mBoundingParameters[1],  this->mBoundingParameters[2]);
			glTexCoord2f(0, 0); glVertex3f(-this->mBoundingParameters[0],  this->mBoundingParameters[1], -this->mBoundingParameters[2]);
			glTexCoord2f(1, 0); glVertex3f( this->mBoundingParameters[0],  this->mBoundingParameters[1], -this->mBoundingParameters[2]);
			glTexCoord2f(1, 1); glVertex3f( this->mBoundingParameters[0],  this->mBoundingParameters[1],  this->mBoundingParameters[2]);

		glEnd();
	}

	glPopMatrix();
}

void GameObject::saveState()
{
	this->mStatePosition =  this->mPosition;
	this->mStateOrientation =  this->mOrientation;
}

void GameObject::loadState()
{
	this->mPosition =  this->mStatePosition;
	this->mOrientation =  this->mStateOrientation;
}

Texture* GameObject::getTexture()
{
	return this->mTexture;
}

void GameObject::setTexture(Texture* texture)
{
	this->mTexture = texture;
}

const btVector3& GameObject::getPosition() const
{
	return this->mPosition;
}

btVector3& GameObject::getPosition()
{
	return this->mPosition;
}

void GameObject::setPosition(const btVector3& p)
{
	this->mPosition = p;
}

const btQuaternion& GameObject::getOrientation() const
{
	return this->mOrientation;
}

btQuaternion& GameObject::getOrientation()
{
	return this->mOrientation;
}

void GameObject::setOrientation(const btQuaternion& o)
{
	this->mOrientation = o;
}

GameObject::BoundingType GameObject::getBoundingType() const
{
	return this->mBoundingType;
}

const float* GameObject::getBoundingParameters() const
{
	return this->mBoundingParameters;
}

void GameObject::setBoundingVolume(GameObject::BoundingType b, float* p)
{
    this->mBoundingType = b;
    if (p != 0)
    {
        switch (this->mBoundingType)
        {
        case GameObject::Sphere:
        case GameObject::Cube:
        {
            this->mBoundingParameters[0] = p[0];
            break;
        }
        case GameObject::Box:
        {
            for (int i = 0; i < 3; i++)
                this->mBoundingParameters[i] = p[i];
            break;
        }
        case GameObject::Brush:
        {
            // not implemented yet
            break;
        }
        }
    }
}

float GameObject::getMass()
{
	return this->mMass;
}

void GameObject::setMass(float mass)
{
	this->mMass = mass;
}

btTransform GameObject::getTransform()
{
	return btTransform(this->mOrientation, this->mPosition);
}
