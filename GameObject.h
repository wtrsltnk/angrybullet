/* 
 * File:   GameObject.h
 * Author: wouter
 *
 * Created on December 14, 2011, 6:10 PM
 */

#ifndef GAMEOBJECT_H
#define	GAMEOBJECT_H

#include "Texture.h"
#include "Tokenizer.h"
#include <LinearMath/btVector3.h>
#include <LinearMath/btQuaternion.h>
#include <LinearMath/btTransform.h>
#include <fstream>

class GameObject
{
public:
	enum BoundingType
	{
		Sphere,	// With radius
		Cube,	// Box with linked width, height, depth
		Box,	// Box with variable width, height, depth
		Brush	// Based on brushes from bsp
	};
	
public:
	GameObject();
	GameObject(Texture* texture);
	GameObject(const GameObject& obj);
	virtual ~GameObject();

	Texture* getTexture();
	void setTexture(Texture* texture);
	
	const btVector3& getPosition() const;
	btVector3& getPosition();
	void setPosition(const btVector3& p);
	
	const btQuaternion& getOrientation() const;
	btQuaternion& getOrientation();
	void setOrientation(const btQuaternion& o);

	BoundingType getBoundingType() const;
	const float* getBoundingParameters() const;
	void setBoundingVolume(BoundingType b, float* p = 0);
	
	float getMass();
	void setMass(float mass);
	
	btTransform getTransform();
	
	virtual void render();
	
	virtual GameObject* clone() = 0;
	void saveState();
	void loadState();
	
    virtual void saveToStream(std::ofstream& stream) = 0;
	virtual void loadFromTokenizer(Tokenizer& tok) = 0;
	
protected:
	btVector3 mPosition;
	btQuaternion mOrientation;
	btVector3 mStatePosition;
	btQuaternion mStateOrientation;
	BoundingType mBoundingType;
	float mBoundingParameters[6];	// Sphere and cube only have one, Box has 3 (half-extends)
	float mMass;
	Texture* mTexture;

};

#endif	/* GAMEOBJECT_H */

