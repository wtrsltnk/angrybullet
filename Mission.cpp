/*
 * File:   Mission.cpp
 * Author: wouter
 *
 * Created on March 24, 2012, 1:17 PM
 */

#include "Mission.h"
#include "Tokenizer.h"
#include "Block.h"
#include "Catapult.h"
#include "Floor.h"
#include "Projectile.h"
#include <fstream>
#include <iostream>
#include <sstream>

Mission::Mission(const std::string& filename)
{
    if (this->load(filename) == false)
        this->floor.setPosition(btVector3(400, 0, 0));

    this->filename = filename;
}

Mission::~Mission()
{
    while (this->objects.empty() == false)
    {
        GameObject* obj = this->objects.back();
        this->objects.pop_back();
        delete obj;
    }
}

bool Mission::save(const std::string& filename)
{
    std::ofstream out(filename.c_str(), std::ios::out);

    if (out.is_open())
    {
        this->floor.saveToStream(out);
        for (auto itr = this->objects.begin(); itr != this->objects.end(); ++itr)
            (*itr)->saveToStream(out);

        out.close();
        return true;
    }
    return false;
}

bool Mission::load(const std::string& filename)
{
    FILE* file = fopen(filename.c_str(), "r");
    if (file != nullptr)
    {
        fseek(file, 0, SEEK_END);
        auto size = ftell(file);
        fseek(file, 0, SEEK_SET);

        if (size > 0)
        {
            char* data = new char[size];
            fread(data, 1, static_cast<size_t>(size), file);

            Tokenizer tok(data, size);

            while (tok.nextToken() && tok.getToken() == std::string("{"))
            {
                tok.nextToken();
                if (tok.getToken() != std::string("class"))
                {
                    std::cout << "Unexpected token: " << tok.getToken() << std::endl;
                    break;
                }
                tok.nextToken();
                if (tok.getToken() == std::string("Block"))
                {
                    Block* obj = new Block();
                    obj->loadFromTokenizer(tok);
                    this->objects.push_back(obj);
                }
                else if (tok.getToken() == std::string("Catapult"))
                {
                    Catapult* obj = new Catapult();
                    obj->loadFromTokenizer(tok);
                    this->objects.push_back(obj);
                }
                else if (tok.getToken() == std::string("Floor"))
                {
                    this->floor.loadFromTokenizer(tok);
                }
                else if (tok.getToken() == std::string("Projectile"))
                {
                    Projectile* obj = new Projectile();
                    obj->loadFromTokenizer(tok);
                    this->objects.push_back(obj);
                }
            }
        }
        fclose(file);
        return true;
    }
    return false;
}

Physics* Mission::start()
{
    Physics* physics = new Physics();
    physics->addObject(&floor);
    for (auto itr = this->objects.begin(); itr != this->objects.end(); ++itr)
        physics->addObject((*itr)->clone());

    return physics;
}

void Mission::stop(Physics*& physics)
{
    physics->clearObjects();
    delete physics;
    physics = 0;
}

void Mission::uploadTextures()
{
    for (auto itr = this->objects.begin(); itr != this->objects.end(); ++itr)
        if ((*itr)->getTexture() != 0)
            (*itr)->getTexture()->upload();
}

void Mission::render()
{
    this->floor.render();
    for (auto itr = this->objects.begin(); itr != this->objects.end(); ++itr)
        (*itr)->render();
}

void Mission::addObject(GameObject* object)
{
    this->objects.push_back(object);
}

GameObject* Mission::pickObject(int mousex, int mousey)
{
    for (auto itr = this->objects.begin(); itr != this->objects.end(); ++itr)
        if (fabs((*itr)->getPosition().x() - mousex) < (*itr)->getBoundingParameters()[2]
                && fabs((*itr)->getPosition().z() - (mousey)) < (*itr)->getBoundingParameters()[0])
            return (*itr);
    return 0;
}

const std::string Mission::getFilename() const
{
    return this->filename;
}

void Mission::setFilename(const std::string& filename)
{
    this->filename = filename;
}

std::string filenameFromIndex(int index)
{
    std::stringstream ss;
    ss << "data/missions/mission" << index << ".txt";
    return ss.str();
}

bool fileExists(std::string& filename)
{
    bool flag = false;
    std::fstream fin;

    fin.open(filename.c_str(), std::ios::in);
    flag = fin.is_open();
    fin.close();

    return flag;
}

std::string Mission::newMissionName()
{
    int index = 0;
    std::string current = filenameFromIndex(index++);
    while (fileExists(current))
    {
        current = filenameFromIndex(index++);
    }
    return current;
}
