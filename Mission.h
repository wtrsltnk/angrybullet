/* 
 * File:   Mission.h
 * Author: wouter
 *
 * Created on March 24, 2012, 1:17 PM
 */

#ifndef MISSION_H
#define	MISSION_H

#include "GameObject.h"
#include "Floor.h"
#include "Physics.h"
#include <vector>
#include <string>

class Mission
{
public:
    Mission(const std::string& filename);
    virtual ~Mission();

    bool save(const std::string& filename);
    bool load(const std::string& filename);

    Physics* start();
    void stop(Physics*& physics);
    void addObject(GameObject* object);
    GameObject* pickObject(int mousex, int mousey);

    void uploadTextures();
    void render();

    const std::string getFilename() const;
    void setFilename(const std::string& filename);

private:
    Floor floor;
    std::vector<GameObject*> objects;
    std::string filename;

public:
    static std::string newMissionName();
};

#endif	/* MISSION_H */

