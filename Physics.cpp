/* 
 * File:   Physics.cpp
 * Author: Administrator
 * 
 * Created on 28 oktober 2010, 2:08
 */

#include "Physics.h"
#include "GameObject.h"
#include "Block.h"
#include "Floor.h"
#include "Catapult.h"
#include <btBulletDynamicsCommon.h>
#include <BulletCollision/CollisionDispatch/btGhostObject.h>
#include <LinearMath/btGeometryUtil.h>
#include <iostream>
#include <vector>

Physics::Config Physics::config = { 0.6f, 0.08f, 0.08f, 9.81f };

class Physics::Implementation
{
public:
    virtual ~Implementation();

    btBroadphaseInterface* mBroadphase;
    btDefaultCollisionConfiguration* mCollisionConfiguration;
    btCollisionDispatcher* mDispatcher;
    btSequentialImpulseConstraintSolver* mSolver;
    btDiscreteDynamicsWorld* mDynamicsWorld;

};

Physics::Implementation::~Implementation()
{ }

GameObjectPhysics::GameObjectPhysics(GameObject* obj)
	: shape(0), object(obj), rigidBody(0)
{
	if (obj != 0)
	{
		this->transform = obj->getTransform();
	}
}

GameObjectPhysics::~GameObjectPhysics()
{ }

void GameObjectPhysics::getWorldTransform(btTransform& worldTrans) const
{
	worldTrans = this->transform;
}

void GameObjectPhysics::setWorldTransform(const btTransform& worldTrans)
{
	this->transform = worldTrans;
}

Physics::Physics()
	: pimpl(new Physics::Implementation())
{
    pimpl->mBroadphase = new btDbvtBroadphase();

    pimpl->mCollisionConfiguration = new btDefaultCollisionConfiguration();
    pimpl->mDispatcher = new btCollisionDispatcher(pimpl->mCollisionConfiguration);

    pimpl->mSolver = new btSequentialImpulseConstraintSolver();

    pimpl->mDynamicsWorld = new btDiscreteDynamicsWorld(pimpl->mDispatcher, pimpl->mBroadphase, pimpl->mSolver, pimpl->mCollisionConfiguration);
    pimpl->mDynamicsWorld->setGravity(btVector3(0, 0, -Physics::config.gravity));
	pimpl->mDynamicsWorld->getBroadphase()->getOverlappingPairCache()->setInternalGhostPairCallback(new btGhostPairCallback());
}

Physics::~Physics()
{
    if (pimpl->mDynamicsWorld != 0)
        delete pimpl->mDynamicsWorld;
	pimpl->mDynamicsWorld = 0;

    if (pimpl->mSolver != 0)
        delete pimpl->mSolver;
	pimpl->mSolver = 0;

    if (pimpl->mDispatcher != 0)
        delete pimpl->mDispatcher;
	pimpl->mDispatcher = 0;

    if (pimpl->mCollisionConfiguration != 0)
        delete pimpl->mCollisionConfiguration;
	pimpl->mCollisionConfiguration = 0;

    if (pimpl->mBroadphase != 0)
        delete pimpl->mBroadphase;
	pimpl->mBroadphase = 0;

	delete pimpl;
}

void Physics::update(float gameTime)
{
    pimpl->mDynamicsWorld->stepSimulation(btScalar(gameTime) * 0.7f, 0);

//	int numManifolds = pimpl->mDynamicsWorld->getDispatcher()->getNumManifolds();
//
//	for (int i = 0; i < numManifolds; i++)
//	{
//		btPersistentManifold* contactManifold = pimpl->mDynamicsWorld->getDispatcher()->getManifoldByIndexInternal(i);
//		btCollisionObject* obA = static_cast<btCollisionObject*>(contactManifold->getBody0());
//		btCollisionObject* obB = static_cast<btCollisionObject*>(contactManifold->getBody1());
//
//		int numContacts = contactManifold->getNumContacts();
//		if (numContacts > 0)
//		{
//			btVector3 position = contactManifold->getContactPoint(0).getPositionWorldOnA();
//			void* shapeA = obA->getUserPointer();
//			void* shapeB = obB->getUserPointer();
//			PhysicsEntity* entityA = 0;
//			PhysicsEntity* entityB = 0;
//
//			if (shapeA != 0) entityA = reinterpret_cast <PhysicsEntity*> (shapeA);
//			if (shapeB != 0) entityB = reinterpret_cast <PhysicsEntity*> (shapeB);
//
//			if (entityA == 0 || entityB == 0)
//				continue;
//
//			if (entityA->isType(EntityTypes::ePhysicsEntity) && entityB->isType(EntityTypes::ePhysicsEntity))
//			{
//				entityA->onCollide(entityB, Vector3(position));
//				entityB->onCollide(entityA, Vector3(position));
//			}
//		}
//
//		contactManifold->clearManifold();
//	}
	for (int i = 0; i < this->pimpl->mDynamicsWorld->getNumCollisionObjects(); i++)
	{
		GameObjectPhysics* physics = (GameObjectPhysics*)this->pimpl->mDynamicsWorld->getCollisionObjectArray()[i]->getUserPointer();
		if (physics != 0 && physics->object != 0)
		{
			physics->object->setPosition(btVector3(physics->transform.getOrigin().x(), physics->transform.getOrigin().y(), physics->transform.getOrigin().z()));
			physics->object->setOrientation(physics->transform.getRotation());
		}
	}
}

void Physics::addObject(GameObject* obj, btVector3 linvec)
{
	GameObjectPhysics* physics = new GameObjectPhysics(obj);

    switch (obj->getBoundingType())
    {
    case GameObject::Sphere:
    {
        float s = obj->getBoundingParameters()[0];
        physics->shape = new btSphereShape(s);
        break;
    }
    case GameObject::Box:
    case GameObject::Cube:
    {
        const float* s = obj->getBoundingParameters();
        physics->shape = new btBoxShape(btVector3(s[0], s[1], s[2]));
        break;
    }
    case GameObject::Brush:
    {
        // not implemented
        break;
    }
    }

    if (physics->shape != 0)
	{
		btVector3 fallInertia(0, 0, 0);
		physics->shape->calculateLocalInertia(obj->getMass(), fallInertia);
		btRigidBody::btRigidBodyConstructionInfo rbci(obj->getMass(), physics, physics->shape, fallInertia);
		physics->rigidBody = new btRigidBody(rbci);
		physics->rigidBody->setUserPointer((void*)physics);
		physics->rigidBody->setLinearFactor(btVector3(1, 0, 1));
		physics->rigidBody->setAngularFactor(btVector3(0, 1, 0));
		physics->rigidBody->setLinearVelocity(linvec);
		this->pimpl->mDynamicsWorld->addRigidBody(physics->rigidBody, (obj->getMass() == 0 ? PhysicsTypes::COL_STATIC : PhysicsTypes::COL_BALL), (obj->getMass() == 0 ? PhysicsTypes::COL_BALL : PhysicsTypes::COL_BALL | PhysicsTypes::COL_STATIC));
	}
}

void Physics::clearObjects()
{
	for (int i = 0; i < this->pimpl->mDynamicsWorld->getNumCollisionObjects(); i++)
	{
		GameObjectPhysics* physics = (GameObjectPhysics*)this->pimpl->mDynamicsWorld->getCollisionObjectArray()[i]->getUserPointer();
		Block* block = dynamic_cast<Block*>(physics->object);
		if (block != 0)
		{
			delete physics->object;
			physics->object = 0;
		}
	}
	
	this->pimpl->mDynamicsWorld->getCollisionObjectArray().clear();
}

void Physics::render()
{
	for (int i = 0; i < this->pimpl->mDynamicsWorld->getNumCollisionObjects(); i++)
		((GameObjectPhysics*)this->pimpl->mDynamicsWorld->getCollisionObjectArray()[i]->getUserPointer())->object->render();
}
