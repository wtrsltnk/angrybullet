/* 
 * File:   Physics.h
 * Author: Administrator
 *
 * Created on 28 oktober 2010, 2:08
 */

#ifndef PHYSICS_H
#define	PHYSICS_H

#include <LinearMath/btMotionState.h>
#include <BulletDynamics/Dynamics/btRigidBody.h>

#define BIT(x) (1<<(x))

namespace PhysicsTypes
{
	enum
	{
		COL_NOTHING = 0,
		COL_BALL = BIT(1),
		COL_STATIC = BIT(2),
		COL_TRIGGER = BIT(3),
		COL_ILLUSIONARY = BIT(4),
		COL_CHARACTER = BIT(5)
	};

};

class GameObject;

class GameObjectPhysics : public btMotionState
{
public:
	GameObjectPhysics(GameObject* obj);
	virtual ~GameObjectPhysics();
	
	virtual void getWorldTransform(btTransform& worldTrans ) const;
	virtual void setWorldTransform(const btTransform& worldTrans);
	
	btCollisionShape* shape;
	GameObject* object;
	btTransform transform;
	btRigidBody* rigidBody;
};

class Physics
{
public:
    static struct Config {
        float resititution;
        float linearDamping;
        float angularDamping;
        float gravity;

    } config;

public:
	Physics();
	virtual ~Physics();

	void update(float gameTime);
	void addObject(GameObject* obj, btVector3 linvec = btVector3(0, 0, 0));
	void clearObjects();
	
	void render();
	
private:
	class Implementation;
	Implementation* pimpl;
	
};

#endif	/* PHYSICS_H */

