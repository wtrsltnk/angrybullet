/* 
 * File:   Projectile.cpp
 * Author: wouter
 * 
 * Created on March 25, 2012, 11:23 AM
 */

#include "Projectile.h"
#include "TextureLoader.h"

Projectile::Projectile()
{ }

Projectile::Projectile(const Projectile& orig)
    : GameObject(orig)
{ }

Projectile::~Projectile()
{ }

void Projectile::saveToStream(std::ofstream& stream)
{
    stream << "{" << std::endl;
    stream << "\"class\" \"Projectile\"" << std::endl;
    stream << "\"mass\" \"" << this->mMass << "\"" << std::endl;
    stream << "\"position\" \""
           << this->getPosition().x()
           << " "  << this->getPosition().y() << " "
           << this->getPosition().z() << "\""
           << std::endl;
    stream << "\"orientation\" \""
           << this->getOrientation().x() << " "
           << this->getOrientation().y() << " "
           << this->getOrientation().z() << " "
           << this->getOrientation().w() << "\""
           << std::endl;
    if (this->getTexture() != 0)
        stream << "\"texture\" \"" << this->getTexture()->mName << "\"" << std::endl;
    stream << "}" << std::endl;
}

void Projectile::loadFromTokenizer(Tokenizer& tok)
{
    while (tok.nextToken() && tok.getToken() != std::string("}"))
	{
        if (tok.getToken() == std::string("mass"))
		{
			this->mMass = atof(tok.getNextToken());
		}
        else if (tok.getToken() == std::string("position"))
		{
			float x = atof(tok.getNextToken());
			float y = atof(tok.getNextToken());
			float z = atof(tok.getNextToken());
			this->mPosition = btVector3(x, y, z);
		}
        else if (tok.getToken() == std::string("orientation"))
		{
			float x = atof(tok.getNextToken());
			float y = atof(tok.getNextToken());
			float z = atof(tok.getNextToken());
			float w = atof(tok.getNextToken());
			this->mOrientation = btQuaternion(x, y, z, w);
		}
        else if (tok.getToken() == std::string("texture"))
		{
			TextureLoader tl;
			this->setTexture(tl.loadFromTga(tok.getNextToken()));
			if (this->getTexture() != 0)
			{
                float parameters[6] = { static_cast<float>(this->getTexture()->mWidth), 50, static_cast<float>(this->getTexture()->mHeight), 0, 0, 0 };
				this->setBoundingVolume(GameObject::Box, parameters);
			}
		}
	}
}

GameObject* Projectile::clone()
{
	return new Projectile(*this);
}
