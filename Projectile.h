/*
 * File:   Projectile.h
 * Author: wouter
 *
 * Created on March 25, 2012, 11:23 AM
 */

#ifndef PROJECTILE_H
#define	PROJECTILE_H

#include "GameObject.h"

class Projectile : public GameObject
{
public:
    Projectile();
    Projectile(const Projectile& orig);
    virtual ~Projectile();

    virtual void saveToStream(std::ofstream& stream);
    virtual void loadFromTokenizer(Tokenizer& tok);
    virtual GameObject* clone();

private:

};

#endif	/* PROJECTILE_H */

