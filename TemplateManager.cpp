/* 
 * File:   TemplateManager.cpp
 * Author: wouter
 * 
 * Created on March 28, 2012, 9:56 PM
 */

#include "TemplateManager.h"
#include <iostream>

#define PI 3.14159265358979323846
#define DEG2RAD(DEG) ((DEG)*((PI)/(180.0)))

TemplateManager::TemplateManager(const std::string& filename)
	: filename(filename), currentIndex(-1), current(0)
{ }

TemplateManager::~TemplateManager()
{ }

void TemplateManager::loadTemplates()
{
	FILE* file = fopen(this->filename.c_str(), "r");
    if (file != nullptr)
    {
		fseek(file, 0, SEEK_END);
		int size = ftell(file);
		fseek(file, 0, SEEK_SET);
		
		if (size > 0)
		{
            char* data = new char[size];
			fread(data, 1, size, file);

			Tokenizer tok(data, size);

            while (tok.nextToken() && tok.getToken() == std::string("{"))
                templates.push_back(Block::fromTemplate(tok));
		}
		fclose(file);
	}
}

void TemplateManager::up()
{
	if (this->current != 0) delete this->current;
	this->current = 0;

	this->currentIndex -= 1;
	if (this->currentIndex < -1) this->currentIndex = this->templates.size() - 1;
	if (this->currentIndex != -1)
	{
		this->current = this->templates[this->currentIndex]->clone();
	}
}

void TemplateManager::down()
{
	if (this->current != 0) delete this->current;
	this->current = 0;

	this->currentIndex += 1;
	if (this->currentIndex >= this->templates.size()) this->currentIndex = -1;
	if (this->currentIndex != -1)
	{
		this->current = this->templates[this->currentIndex]->clone();
	}
}
