/* 
 * File:   TemplateManager.h
 * Author: wouter
 *
 * Created on March 28, 2012, 9:56 PM
 */

#ifndef TEMPLATEMANAGER_H
#define	TEMPLATEMANAGER_H

#include <vector>
#include <string>
#include "Block.h"

class TemplateManager
{
public:
    TemplateManager(const std::string& filename);
    virtual ~TemplateManager();

    void loadTemplates();

    void up();
    void down();

    std::vector<Block*> templates;
    int currentIndex;
    GameObject* current;
private:
    std::string filename;

};

#endif	/* TEMPLATEMANAGER_H */

