/* 
 * File:   TextureLoader.h
 * Author: wouter
 *
 * Created on November 27, 2010, 3:45 PM
 */

#ifndef TEXTURELOADER_H
#define	TEXTURELOADER_H

#include "Texture.h"
#include "FileLoader.h"
#include <string>

class TextureLoader : public FileLoader
{
public:
    TextureLoader();
    virtual ~TextureLoader();

    Texture* loadTexture(const std::string& filename);
    Texture* loadFromTga(const std::string& textureName);
    Texture* loadFromTga(const char* data, int size);

    bool writeTGA(const std::string& file, const Texture* texture);

};

#endif	/* TEXTURELOADER_H */

