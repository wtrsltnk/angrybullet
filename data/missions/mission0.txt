{
"class" "Floor"
"mass" "0"
"position" "400 0 0"
"orientation" "0 0 0 1"
}
{
"class" "Block"
"mass" "1"
"position" "590 0 122"
"orientation" "0 0.707107 0 0.707107"
"texture" "data/wood.tga"
"material" "wood"
}
{
"class" "Block"
"mass" "1"
"position" "715 0 123"
"orientation" "0 0.707107 0 0.707107"
"texture" "data/wood.tga"
"material" "wood"
}
{
"class" "Block"
"mass" "1"
"position" "648 0 212"
"orientation" "0 1 0 -1.62921e-07"
"texture" "data/wood.tga"
"material" "wood"
}
{
"class" "Block"
"mass" "1"
"position" "783 0 211"
"orientation" "0 1 0 -1.62921e-07"
"texture" "data/wood.tga"
"material" "wood"
}
{
"class" "Block"
"mass" "1"
"position" "851 0 125"
"orientation" "0 0.707107 0 0.707106"
"texture" "data/wood.tga"
"material" "wood"
}
{
"class" "Block"
"mass" "1"
"position" "985 0 126"
"orientation" "0 0.707107 0 0.707106"
"texture" "data/wood.tga"
"material" "wood"
}
{
"class" "Block"
"mass" "1"
"position" "919 0 211"
"orientation" "0 0 0 1"
"texture" "data/wood.tga"
"material" "wood"
}
{
"class" "Block"
"mass" "5"
"position" "653 0 313"
"orientation" "0 0.707107 0 0.707107"
"texture" "data/stone.tga"
"material" "stone"
}
{
"class" "Block"
"mass" "5"
"position" "788 0 309"
"orientation" "0 0.707107 0 0.707107"
"texture" "data/stone.tga"
"material" "stone"
}
{
"class" "Block"
"mass" "5"
"position" "920 0 308"
"orientation" "0 0.707107 0 0.707107"
"texture" "data/stone.tga"
"material" "stone"
}
{
"class" "Block"
"mass" "5"
"position" "857 0 408"
"orientation" "0 0 0 1"
"texture" "data/stone.tga"
"material" "stone"
}
{
"class" "Block"
"mass" "5"
"position" "718 0 411"
"orientation" "0 0 0 1"
"texture" "data/stone.tga"
"material" "stone"
}
{
"class" "Block"
"mass" "5"
"position" "857 0 505"
"orientation" "0 0.707107 0 0.707107"
"texture" "data/stone.tga"
"material" "stone"
}
{
"class" "Block"
"mass" "5"
"position" "723 0 510"
"orientation" "0 0.707107 0 0.707107"
"texture" "data/stone.tga"
"material" "stone"
}
{
"class" "Block"
"mass" "5"
"position" "790 0 595"
"orientation" "0 0 0 1"
"texture" "data/stone.tga"
"material" "stone"
}
