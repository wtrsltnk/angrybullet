#define GLEXTL_IMPLEMENTATION
#include <GL/glextl.h>

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <iostream>
#include <stdio.h>

#include "Physics.h"
#include "GameObject.h"
#include "Block.h"
#include "Floor.h"
#include "Catapult.h"
#include "TextureLoader.h"
#include "Tokenizer.h"
#include "Physics.h"
#include "Mission.h"
#include "TemplateManager.h"

static int windowWidth = 800;
static int windowHeight = 600;

static bool drag = false;
static int dragX = 0;
static int dragY = 0;

static int offset = 0;

static GameObject* selected = 0;

#define PI 3.14159265358979323846
#define DEG2RAD(DEG) ((DEG)*((PI)/(180.0)))

void APIENTRY gluLookAt( GLdouble eyex, GLdouble eyey, GLdouble eyez,
                         GLdouble centerx, GLdouble centery, GLdouble centerz,
                         GLdouble upx, GLdouble upy, GLdouble upz )
{
   GLdouble m[16];
   GLdouble x[3], y[3], z[3];
   GLdouble mag;
   /* Make rotation matrix */
   /* Z vector */
   z[0] = eyex - centerx;
   z[1] = eyey - centery;
   z[2] = eyez - centerz;
   mag = sqrt( z[0]*z[0] + z[1]*z[1] + z[2]*z[2] );
   if (mag != 0.0) {  /* mpichler, 19950515 */
      z[0] /= mag;
      z[1] /= mag;
      z[2] /= mag;
   }
   /* Y vector */
   y[0] = upx;
   y[1] = upy;
   y[2] = upz;
   /* X vector = Y cross Z */
   x[0] =  y[1]*z[2] - y[2]*z[1];
   x[1] = -y[0]*z[2] + y[2]*z[0];
   x[2] =  y[0]*z[1] - y[1]*z[0];
   /* Recompute Y = Z cross X */
   y[0] =  z[1]*x[2] - z[2]*x[1];
   y[1] = -z[0]*x[2] + z[2]*x[0];
   y[2] =  z[0]*x[1] - z[1]*x[0];
   /* mpichler, 19950515 */
   /* cross product gives area of parallelogram, which is < 1.0 for
    * non-perpendicular unit-length vectors; so normalize x, y here
    */
   mag = sqrt( x[0]*x[0] + x[1]*x[1] + x[2]*x[2] );
   if (mag != 0.0) {
      x[0] /= mag;
      x[1] /= mag;
      x[2] /= mag;
   }
   mag = sqrt( y[0]*y[0] + y[1]*y[1] + y[2]*y[2] );
   if (mag != 0.0) {
      y[0] /= mag;
      y[1] /= mag;
      y[2] /= mag;
   }
#define M(row,col)  m[col*4+row]
   M(0,0) = x[0];  M(0,1) = x[1];  M(0,2) = x[2];  M(0,3) = 0.0;
   M(1,0) = y[0];  M(1,1) = y[1];  M(1,2) = y[2];  M(1,3) = 0.0;
   M(2,0) = z[0];  M(2,1) = z[1];  M(2,2) = z[2];  M(2,3) = 0.0;
   M(3,0) = 0.0;   M(3,1) = 0.0;   M(3,2) = 0.0;   M(3,3) = 1.0;
#undef M
   glMultMatrixd( m );
   /* Translate Eye to Origin */
   glTranslated( -eyex, -eyey, -eyez );
}

int main(int argc, char* argv[])
{
    std::string missionName = Mission::newMissionName();
	if (argc > 2)
	{
		for (int i = 1; i < argc; i++)
		{
            if (argv[i] == std::string("--mission") && i+1 < argc)
				missionName = argv[i+1];
		}
	}
	
	TemplateManager tm("data/templates.editor");
	Mission mission(missionName);
	Physics* physics = 0;
	
	Catapult catapult;
	catapult.setPosition(btVector3(400, 0, 160));
	
    sf::RenderWindow App(sf::VideoMode(static_cast<unsigned int>(windowWidth), static_cast<unsigned int>(windowHeight), 32), "Angry Birds");

    tm.loadTemplates();

    App.setFramerateLimit(60);

    mission.uploadTextures();

    for (auto itr = tm.templates.begin(); itr != tm.templates.end(); ++itr)
		(*itr)->getTexture()->upload();
	
    glClearDepth(1.f);
    glClearColor(0.f, 0.f, 0.f, 0.f);

    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
	glOrtho(0, 800, 0, 600, -500, 500);

    while (App.isOpen())
    {
        sf::Event Event;
        while (App.pollEvent(Event))
        {
            if (Event.type == sf::Event::Closed)
                App.close();

            if ((Event.type == sf::Event::KeyPressed)
                    && (Event.key.code == sf::Keyboard::Escape))
                App.close();
			
            if ((Event.type == sf::Event::KeyPressed)
                    && (Event.key.code == sf::Keyboard::Space))
			{
				if (physics == 0)
					physics = mission.start();
				else
					mission.stop(physics);
			}
			
			if (physics == 0)	// Editing mode
			{
                if ((Event.type == sf::Event::KeyPressed)
                        && (Event.key.code == sf::Keyboard::Up))
					tm.up();

                if ((Event.type == sf::Event::KeyPressed)
                        && (Event.key.code == sf::Keyboard::Down))
					tm.down();

                if ((Event.type == sf::Event::KeyPressed)
                        && (Event.key.code == sf::Keyboard::Right))
				{
					GameObject* obj = tm.current;
					if (obj == 0) obj = selected;
					
					if (obj != 0)
					{
						btQuaternion q = obj->getTransform().getRotation();
						btScalar angle = q.getAngle();
						if (angle < DEG2RAD(179.0f))
							angle += DEG2RAD(10);
						q.setRotation(btVector3(0, 1, 0), angle);
						obj->setOrientation(q);
					}
				}

                if ((Event.type == sf::Event::KeyPressed)
                        && (Event.key.code == sf::Keyboard::Left))
				{
					GameObject* obj = tm.current;
					if (obj == 0) obj = selected;
					
					if (obj != 0)
					{
						btQuaternion q = obj->getTransform().getRotation();
						btScalar angle = q.getAngle();
						if (angle > 0)
							angle -= DEG2RAD(10);
						q.setRotation(btVector3(0, 1, 0), angle);
						obj->setOrientation(q);
					}
				}

                if (Event.type == sf::Event::MouseButtonReleased
                        && Event.mouseButton.button == sf::Mouse::Left)
				{
					if (tm.current != 0)
						mission.addObject(tm.current->clone());
					else
						selected = 0;
				}

                if (Event.type == sf::Event::MouseButtonPressed
                        && Event.mouseButton.button == sf::Mouse::Left)
				{
					if (tm.current == 0)
					{
                        selected = mission.pickObject(Event.mouseButton.x-offset, windowHeight - Event.mouseButton.y);
					}
				}

                if (Event.type == sf::Event::MouseMoved)
				{
					if (tm.current != 0)
                        tm.current->setPosition(btVector3(Event.mouseMove.x-offset, 0, windowHeight - Event.mouseMove.y));
					else if (selected != 0)
                        selected->setPosition(btVector3(Event.mouseMove.x-offset, 0, windowHeight - Event.mouseMove.y));
				}

                if ((Event.type == sf::Event::KeyPressed)
                        && (Event.key.code == sf::Keyboard::S
                            && Event.key.control
                            && !Event.key.shift))
				{
					mission.save(mission.getFilename().c_str());
				}

                if ((Event.type == sf::Event::KeyPressed)
                        && (Event.key.code == sf::Keyboard::S
                            && Event.key.control && Event.key.shift))
				{
					mission.setFilename(Mission::newMissionName());
					mission.save(mission.getFilename().c_str());
				}
			}
			else	// Game mode
			{
                if (Event.type == sf::Event::MouseButtonPressed
                        && Event.mouseButton.button == sf::Mouse::Left)
				{
                    if (fabs(catapult.getPosition().x() - Event.mouseButton.x+offset) < 50
                            && fabs(catapult.getPosition().z() - (windowHeight-Event.mouseButton.y)) < 50)
					{
                        catapult.capture(Event.mouseButton.x-offset, windowHeight-Event.mouseButton.y);
					}
				}

                if (Event.type == sf::Event::MouseButtonReleased
                        && Event.mouseButton.button == sf::Mouse::Left)
				{
					if (catapult.isCaptured() && physics != 0)
					{
						btVector3 linvec;
						GameObject* obj = catapult.uncapture(linvec);
						physics->addObject(obj, linvec*-1.0f);
					}
				}
			}
			
			// Common
            if (Event.type == sf::Event::MouseButtonPressed
                    && Event.mouseButton.button == sf::Mouse::Middle)
			{
				drag = true;
                dragX = Event.mouseButton.x;
                dragY = Event.mouseButton.y;
			}
			
            if (Event.type == sf::Event::MouseButtonReleased
                    && Event.mouseButton.button == sf::Mouse::Middle)
			{
				drag = false;
			}
			
            if (Event.type == sf::Event::MouseMoved)
			{
				if (catapult.isCaptured())
				{
                    catapult.move(Event.mouseMove.x-offset, windowHeight-Event.mouseMove.y);
				}
				else if (drag == true)
				{
                    offset += (Event.mouseMove.x-dragX);
                    dragX = Event.mouseMove.x;
                    dragY = Event.mouseMove.y;
				}
			}

            if (Event.type == sf::Event::Resized)
			{
                glViewport(0, 0, int(Event.size.width), int(Event.size.height));

                windowWidth = int(Event.size.width);
                windowHeight = int(Event.size.height);
				if (windowHeight <= 0) windowHeight = 1;
				
				glMatrixMode(GL_PROJECTION);
				glLoadIdentity();
				glOrtho(0, windowWidth, 0, windowHeight, -500, 500);
			}
        }
		
		if (physics != 0)
			physics->update(3.0f/30.0f);

        App.setActive();
		
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        gluLookAt(0,-250,0, 0, 0, 0, 0, 0, 1);
		
		glTranslatef(offset, 0, 0);
	
		catapult.render();
		
		if (tm.current != 0 && physics == 0)
			tm.current->render();
		
		if (physics == 0)
			mission.render();
		else
			physics->render();
		
        App.display();
    }

    return EXIT_SUCCESS;
}
